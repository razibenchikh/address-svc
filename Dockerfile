FROM openjdk:8-jdk-alpine

COPY target/*.jar adresse.jar

ENTRYPOINT ["java","-jar","/adresse.jar"]